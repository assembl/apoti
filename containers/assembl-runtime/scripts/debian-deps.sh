#!/bin/bash
export DEBIAN_FRONTEND="non-interactive"

apt update
apt -y install curl apt-transport-https software-properties-common gnupg2
apt -y purge cmdtest
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee -a /etc/apt/sources.list.d/yarn.list
apt update

apt -y install \
    yarn=1.17.3-1 python-virtualenv \
    python-pip python-psycopg2 \
    build-essential automake git \
    bison flex gawk libpq-dev \
    libzmq3-dev libxslt1-dev \
    libffi-dev libxml2-dev \
    libssl-dev libreadline-dev git \
    libxmlsec1-dev libgraphviz-dev \
    libpq-dev libmemcached-dev libzmq3-dev \
    libxslt1-dev libffi-dev libhiredis-dev libxml2-dev libssl-dev \
    libreadline-dev libxmlsec1-dev libcurl4-openssl-dev
