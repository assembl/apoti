#!/bin/bash
set -x
virtualenv -p python2.7 /venv
source /venv/bin/activate || exit 1
pip install -U pip
pip install psycopg2
pip install -U setuptools "pip<10"
pip install lxml
pip install dm.xmlsec.binding
pip install pycurl
pip install nodeenv
# deploy node 10.13.0
nodeenv --node=10.13.0 --npm=6.4.1 --python-virtualenv /assembl/assembl/static/js
npm install -g reinstall

# npm install -g eslint~4.12.0 stylelint^10.1.0 jest^23.6.0
