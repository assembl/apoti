#!/bin/bash

ASSEMBL_GIT_REF=$1
APOTI_DOCKER_TAG=$2

ENABLE_CDN=${ENABLE_CDN:-}
errout() {
    print $@
    exit 1
}

test -z "$APOTI_DOCKER_TAG" && errout "Usage: $0 ASSEMBL_GIT_REF APOTI_DOCKER_TAG"
set -x
make ENABLE_CDN=$ENABLE_CDN ASSEMBL_GIT_REF=$ASSEMBL_GIT_REF APOTI_DOCKER_TAG=$APOTI_DOCKER_TAG  assembl-build assembl-push || errout "Build failed"
#make ASSEMBL_GIT_REF=$ASSEMBL_GIT_REF APOTI_DOCKER_TAG=$APOTI_DOCKER_TAG  url_metadata
set +x
