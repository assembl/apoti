#!/bin/bash
errout() {
    echo $@
    exit 1
}

test -f /backup-config || errout "missing /backup-config"
test -f /run/secrets/backup-secrets* || errout "missing backup-secrets secret"

set -a
source /backup-config
source /run/secrets/backup-secrets*
set +a

ACTION=$1
ARCHIVE_TARGET=$2

create_archive_folder() {
    mkdir -p /backups/workdir/archives/${ARCHIVE_TARGET}
    date +%Y%m%d_%H%M%S_%Z > /backups/workdir/archives/${ARCHIVE_TARGET}/01-date-start
}

finish_archive_folder() {
    test -d /backups/workdir/archives/${ARCHIVE_TARGET}
    date +%Y%m%d_%H%M%S_%Z > /backups/workdir/archives/${ARCHIVE_TARGET}/02-date-end
}

case ${ACTION} in
    sleep)
        mkfifo "/.pause.fifo" 2>/dev/null; read <"/.pause.fifo"
        ;;
    start-backup)
        echo "starting backup process"
        create_archive_folder
        echo "done. please proceed."
        ;;
    backup-db)
        echo "backing up database"
        pg_dumpall --no-owner --clean --if-exists > /backups/workdir/archives/${ARCHIVE_TARGET}/database.sql
        echo "done"
        ;;
    backup-assets)
        echo "backing up assets"
        aws s3 sync $S3_TARGET_URL /backups/workdir/archives/${ARCHIVE_TARGET}/assets
        echo "done"
        ;;
    make-archive-file)
        echo "making archive file"
        cd /backups/workdir/archives/${ARCHIVE_TARGET}
        mkdir -p /backups/archives
        finish_archive_folder;
        tar cvzf /backups/archives/${ARCHIVE_TARGET}.tgz .
        echo "done"
        ;;
    restore-archive-file)
        nowdate=$(date +%s)
        test -f /backups/archives/${ARCHIVE_TARGET}.tgz || exit 1
        rm -rf /backups/workdir/restore
        mkdir -p /backups/workdir/restore
        echo "Keeping before-restoration data in /backups/workdir/before-restore/${nowdate}"
        mkdir -p /backups/workdir/before-restore/${nowdate}
        pg_dumpall -c > /backups/workdir/before-restore/${nowdate}/database.sql
        aws s3 sync $S3_TARGET_URL /backups/workdir/before-restore/${nowdate}/assets
        echo "Now restoring"
        cd /backups/workdir/restore
        tar xvf /backups/archives/${ARCHIVE_TARGET}.tgz
        test -f database.sql && psql < database.sql
        test -f database.pgdump && pg_restore database.pgdump
        cd assets
        aws s3 sync . $S3_TARGET_URL
        echo "done restoring"
        ;;
    **|help)
        cat <<EOF
Usage:
    sleep                               Sleep forever
    start-backup ARCHIVE_NAME           Start a backup process (timestamp it)
    backup-db ARCHIVE_NAME              Backup the database as-is to archive
    backup-assets ARCHIVE_NAME          Backup the assets
    make-archive-file ARCHIVE_NAME      Finalize the archive. This makes the
                                            actual archive file
    restore-archive-file ARCHIVE_NAME   Restore a previously created/added
                                            archive file that is present
                                            in /backups
EOF
        exit 1
        ;;
esac
