#!/usr/bin/env python

from __future__ import print_function
import ConfigParser
import sys


if len(sys.argv) == 1:
    files = ['/app/uwsgi.ini']
else:
    files = [str(a) for a in sys.argv[1:]]

c = ConfigParser.SafeConfigParser()
c.read(files)

d = {}

d['CELERY_TASKS_BROKER'] = c.get('app:assembl', 'celery_tasks.broker')

print("\n".join('{}="{}"'.format(a, b) for a, b in d.items()))
print("\n".join("export {}".format(a) for a in d))
