#!/bin/bash
source /venv/bin/activate
cd /app

echo "* generating translations *"
python assembl/scripts/po2json.py
python setup.py compile_catalog

echo "* building front-end static2 *"

pushd /app/assembl/static2
yarn do-ci || (yarn build:raw --config override.webpack.js && yarn build:cleanup --config override.webpack.js)
popd

echo "* building front-end static *"

pushd /app/assembl/static/js
yarn
node_modules/.bin/gulp libs
node_modules/.bin/gulp sass
node_modules/.bin/gulp browserify:prod
node_modules/.bin/node-sass --source-map -r -o /app/assembl/static/widget/card/app/css --source-map /app/assembl/static/widget/card/app/css assembl/static/widget/card/app/scss
node_modules/.bin/node-sass --source-map -r -o /app/assembl/static/widget/video/app/css --source-map /app/assembl/static/widget/video/app/css assembl/static/widget/video/app/scss
node_modules/.bin/node-sass --source-map -r -o /app/assembl/static/widget/session/css --source-map /app/assembl/static/widget/session/css assembl/static/widget/session/scss
popd

