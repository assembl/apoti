#!/bin/bash
set -x
export NODE_ENV=development
echo "* install widgets that depend on the bower stack *"

if [ -z "$static_widgets" ]; then
    echo "static_widgets undefined"
    exit 1
fi

source /venv/bin/activate
cd /app/assembl/static/js
npm install -g bower@1.8.8 po2json requirejs
# maybe we need to npm install too
npm install
echo '{ "allow_root": true }' > ~/.bowerrc
for i in $static_widgets; do 
    pushd /app/assembl/static/widget/$i
    bower prune
    bower install --force --allow-root
    popd
done
cd /app
bower install --allow-root
