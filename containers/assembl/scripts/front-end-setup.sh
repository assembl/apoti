#!/bin/bash

echo "* install front-ends static and static2 using yarn *"
source /venv/bin/activate
pushd /app/assembl/static/js
npm update
pushd /app/assembl/static2
rm -rf node_modules
cat > override.webpack.js <<'EOF'
const c = require('./webpack.config');
const theCdn = process.env.STATIC2_URL || '';
if (process.env.ENABLE_CDN === "1") {
    let pp = theCdn;
    if (!pp.endsWith('/')) {
        pp += '/';
    }
    c.output.publicPath = pp;
}
module.exports = c;
EOF
yarn

