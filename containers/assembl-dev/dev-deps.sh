#!/bin/bash
source /venv/bin/activate
echo "Installing testing and develop dependencies for pyramid"
cd /app
pip install -r requirements-dev.frozen.txt
pip install -r requirements-tests.frozen.txt
rm -f /tmp/p.sh
