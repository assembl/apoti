#!/bin/bash
echo "* install widgets that depend on the bower stack *"

if [ -z "$static_widgets" ]; then
    echo "static_widgets undefined"
    exit 1
fi

source /venv/bin/activate
cd /app/assembl/static/js
npm install -g bower po2json requirejs
for i in $static_widgets; do 
    pushd /app/assembl/static/widget/$i
    bower prune
    bower install --allow-root --force
    popd
done
cd /app
bower install --allow-root
