#!/bin/bash
echo "* prepare useful runtime directories *"
mkdir -p /app/var/{log,run,db,share,uploads}

source /venv/bin/activate
cd /app
# deploy the pseudo-egg assembl
echo "* deploy the app as an egg *"
pip install -e ./

