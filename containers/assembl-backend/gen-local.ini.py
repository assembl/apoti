#!/usr/bin/env python
"""assembl v2 gets its configuration from the uwsgi config. this means we must
   generate a uwsgi config that provides the right values and works with this
   docker container's expectations regarding socket paths and permissions.

   This script is called upon startup and will generate /app/uwsgi.ini

   Inputs: environment, /secrets.cfg
"""

import os
import ConfigParser
import glob

CONFIG_FILES = ("/config/boilerplate.ini",
                "/config/defaults.ini",
                "/config/aws-defaults.ini", "/config/handlers-logging.ini",
                "/config/language.ini", "/config/security.ini",
                "/config/services.ini", "/config/uwsgi-defaults.ini",
                ) + tuple(glob.glob("/config/overrides/*.ini")) + tuple(glob.glob("/run/secrets/assembl-*.ini"))

cfg = ConfigParser.SafeConfigParser()
cfg.optionxform = lambda x: x
cfg.read(CONFIG_FILES)
with open('/app/uwsgi.ini', 'w') as fh:
    fh.write("# generated from {}\n".format(str(CONFIG_FILES)))
    cfg.write(fh)
