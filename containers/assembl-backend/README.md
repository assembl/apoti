# Theming

- Put your v1 themes in themes/v1
- Put your v2 themes in themes/v2

# Configuring requirements
- Put a requirements-frozen.txt file in config/
- Won't work otherwise

# Runtime: provide secrets and other configs safely

- Write your secrets and alt. config in a config.ini file
- Bind it to /tmp/config.ini
 
