#!/bin/bash

echo "*** generate local.ini from parameters ***"

python /gen-local.ini.py

eval $(python /populate-environment.py /app/uwsgi.ini)

mkdir -p /app/var/log
source /venv/bin/activate
export
/start.sh $@
export
