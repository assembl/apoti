#!/bin/bash
# change this to a path that makes sense on the docker server.
if [ -z "${ASSEMBL_STACKS_PATH}" -o -z "${ASSEMBL_STACK_NAME}" ]; then
    printf "You need to set the following environment variables:\n\t ASSEMBL_STACKS_PATH ASSEMBL_STACK_NAME\n"
    exit 1
fi
echo "Type 'DELETE ME' to delete ${ASSEMBL_STACKS_PATH}/${ASSEMBL_STACK_NAME}" 

read DODELETE

if [ "${DODELETE}" != "DELETE ME" ]; then 
echo "not deleting"
exit 0
fi

cd ${ASSEMBL_STACKS_PATH}/${ASSEMBL_STACK_NAME}
docker-compose down
docker volume rm ${ASSEMBL_STACK_NAME}_metadata-db
docker volume rm ${ASSEMBL_STACK_NAME}_config-overrides
docker volume rm ${ASSEMBL_STACK_NAME}_attachments
docker volume rm ${ASSEMBL_STACK_NAME}_redis-data
