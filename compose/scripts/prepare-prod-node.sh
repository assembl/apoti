#!/bin/bash
echo "* prepare a node for prod-ish Assembl-Apoti workflows *"
echo "Press ENTER to continue or ^C to get out"
read _whatevs;

echo "* creating net-traefik"
docker network create net-traefik
echo "* starting traefik with our desired options"
docker run -v "/var/run/docker.sock:/var/run/docker.sock:ro" -p 80:80 -p 8080:8080 -l "traefik.enable=true" -l "traefik.port=8080" -l "traefik.backend=traefik" -l 'traefik.frontend.rule=Host(`traefik.localhost`)' --network net-traefik --restart=always --name=traefik-rp -d traefik:v2.0.0-rc3 --api.insecure=true --providers.docker=true --providers.docker.exposedByDefault=false --entrypoints.web.address=:80
