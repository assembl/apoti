#!/bin/bash
# change this to a path that makes sense on the docker server.
if [ -z "${ASSEMBL_STACKS_PATH}" -o -z "${ASSEMBL_STACK_NAME}" ]; then
    printf "You need to set the following environment variables:\n\t ASSEMBL_STACKS_PATH ASSEMBL_STACK_NAME\n"
    exit 1
fi
mkdir -p ${ASSEMBL_STACKS_PATH}/${ASSEMBL_STACK_NAME}
cp docker-compose.prod-ready.yml ${ASSEMBL_STACKS_PATH}/${ASSEMBL_STACK_NAME}/docker-compose.yml
docker volume create ${ASSEMBL_STACK_NAME}_metadata-db
docker volume create ${ASSEMBL_STACK_NAME}_config-overrides
docker volume create ${ASSEMBL_STACK_NAME}_attachments
docker volume create ${ASSEMBL_STACK_NAME}_redis-data
cd ${ASSEMBL_STACKS_PATH}/${ASSEMBL_STACK_NAME}
cat > env.sh <<EOF
#!/bin/bash
export ASSEMBL_PUBLIC_HOSTNAME="${ASSEMBL_PUBLIC_HOSTNAME}"
export ASSEMBL_STACKS_PATH="${ASSEMBL_STACKS_PATH}"
export ASSEMBL_STACK_NAME="${ASSEMBL_STACK_NAME}"
EOF
docker-compose up -d
docker-compose logs
