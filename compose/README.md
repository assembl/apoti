# Design

lets. be. simple.

2 modes of operation:

* docker-compose -> the assembl source tree is mounted from your local filesystem. Slight performance impact on mac.

* docker-swarm -> all is embedded in the docker images. Deploy a stack using the ansible system at https://gitlab.com/assembl/system-administration/apoti

# Devmode details

We assume apoti is cloned in `~/dev/apoti` and a reasonably recent assembl (min tag 3.2.8) in `~/dev/assembl`

## Login to Docker

```bash
docker login registry.gitlab.com
```

Use your credentials.

## Build your containers

```bash
cd ~/dev/apoti/containers
make ASSEMBL_VERSION=your_branch build push
```

## Deploy apoti

```bash
export ASSEMBL_DEV_PORT=23788
export ASSEMBL_VERSION=3.2.8
export ASSEMBL_GIT_REPO=~/dev/assembl
docker-compose up -d
docker-compose exec assembl /build-apoti.sh
docker-compose exec assembl bash -c 'cd /app; source /venv/bin/activate; supervisorctl start uwsgi'
```

Then try to access at http://assembl.localhost:23788 or http://assembl.127.0.0.1.xip.io:23788

If you don't use a shared machine just use `export ASSEMBL_DEV_PORT=80` and access http://assembl.localhost or http://assembl.127.0.0.1.xip.io.

In order for `*.localhost` to work, use 1.1.1.1 as your primary DNS resolver.

## Manage apoti

Drop to a shell in the assembl container:

```bash
export ASSEMBL_DEV_PORT=23788
export ASSEMBL_VERSION=3.2.8
export ASSEMBL_GIT_REPO=~/dev/assembl
docker-compose exec assembl bash
```

Then do as follows in the container:

```bash
source /venv/bin/activate
cd /app
supervisorctl restart uwsgi
```

each time you want to test your changes, restart uwsgi. If you need to see the logs, they are in the `/logs` directory.
