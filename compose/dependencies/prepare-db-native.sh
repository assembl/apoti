#!/bin/bash
apt -y install postgresql-10
if [ -z "${ASSEMBL_STACKS_PATH}" -o -z "${ASSEMBL_STACK_NAME}" ]; then
    printf "You need to set the following environment variables:\n\t ASSEMBL_STACKS_PATH ASSEMBL_STACK_NAME\n"
    exit 1
fi
su - postgres <<EOF
pg_createcluster 10 ${ASSEMBL_STACK_NAME}
EOF
