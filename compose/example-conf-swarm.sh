export ASSEMBL_STACKS_PATH="$HOME/assembl"
export ASSEMBL_STACK_NAME="demo-apoti"
export ASSEMBL_PUBLIC_HOSTNAME="${ASSEMBL_STACK_NAME}.localhost"
export ASSEMBL_TRAEFIK_RULES="Host(\`assembl.localhost\`) || Host(\`$ASSEMBL_STACK_NAME.cloud.bluenove.com\`)"
